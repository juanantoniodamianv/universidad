#Proyecto: Posadas Ciudad Universitaria
El siguiente es un proyecto presentado en la Hackathon Posadas Mayo/2017.
Tiene como objetivos los siguientes items:

##Objetivos generales: 
* Desarrollar una aplicación web, que permita a los estudiantes de Posadas acceder a información sobre lo que ofrece la Agencia Universitaria para ellos, recibir información específica relacionada a su ámbito institucional, y sobre todas aquellas actividades -deportivas, culturales, recreativas- que necesiten para disfrutar más de Posadas como Ciudad Universitaria. 

##Objetivos Específicos:
* Captar la mayor cantidad de estudiantes, tanto del nivel secundario como universitario, para que accedan a esta aplicación web, para conocer los servicios que la Agencia Universitaria pone a disposición de ellos.
* Asesorar a interesados, sobre las opciones académica que tiene la ciudad de Posadas, donde se pueden cursar, cuánto duran y qué perfil tienen esas carreras.
* Proveer datos relacionados al ámbito académico específico de los estudiantes, es decir, que pueda acceder a información específica a su campo de conocimiento: eventos, congresos, seminarios, cursos, talleres, etc.

(Lo desarrollado hasta el momento es únicamente un prototipo.)