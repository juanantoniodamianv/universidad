class AddAddressToStudent < ActiveRecord::Migration[5.0]
  def change
  	add_column :students, :first_name, :string
  	add_column :students, :last_name, :string
  	add_column :students, :dni_number, :string
  	add_column :students, :cuil_number, :string
  	add_column :students, :birthday, :datetime
  	add_column :students, :address, :string
  	add_column :students, :telephone, :string
  end
end
